const setTextureStyle = (canvasObject, scaleMode, mipmapMode) => canvasObject.texture.baseTexture.setStyle(PIXI.SCALE_MODES[scaleMode], PIXI.MIPMAP_MODES[mipmapMode]).update();

const canUpdateBackground = () => game.settings.get("scene-image-interpolation-settings", "background-enable") && "undefined" !== typeof canvas.environment.primary.background;
const setBackgroundScaleMode = (settingsValue) => setTextureStyle(canvas.environment.primary.background, settingsValue, game.settings.get("scene-image-interpolation-settings", "background-mipmap-mode"));
const setBackgroundMipmapMode = (settingsValue) => setTextureStyle(canvas.environment.primary.background, game.settings.get("scene-image-interpolation-settings", "background-scale-mode"), settingsValue);

const canUpdateForeground = () => game.settings.get("scene-image-interpolation-settings", "foreground-enable") && "undefined" !== typeof canvas.environment.primary.foreground;
const setForegroundScaleMode = (settingsValue) => setTextureStyle(canvas.environment.primary.foreground, settingsValue, game.settings.get("scene-image-interpolation-settings", "foreground-mipmap-mode"));
const setForegroundMipmapMode = (settingsValue) => setTextureStyle(canvas.environment.primary.foreground, game.settings.get("scene-image-interpolation-settings", "foreground-scale-mode"), settingsValue);

const setEnvironmentFXAA = (enabled) => {
    const fxaaFilter = canvas.environment.filters.find((filter) => filter instanceof AdaptiveFXAAFilter);

    if ("undefined" !== typeof fxaaFilter) {
        fxaaFilter.enabled = enabled;
    }
};

const updateSceneImageRenderingSettings = function(useDefaults) {
    setEnvironmentFXAA(game.settings.get("scene-image-interpolation-settings", "environment-fxaa-enable"));

    if ("undefined" !== typeof canvas.environment.primary.background) {
        if (game.settings.get("scene-image-interpolation-settings", "background-enable")) {
            setTextureStyle(canvas.environment.primary.background, game.settings.get("scene-image-interpolation-settings", "background-scale-mode"), game.settings.get("scene-image-interpolation-settings", "background-mipmap-mode"));
        } else if (useDefaults) {
            // Default values in FVTT 0.8.9.
            canvas.environment.primary.background.texture.baseTexture.setStyle(PIXI.SCALE_MODES.LINEAR, PIXI.MIPMAP_MODES.OFF).update();
        }
    }

    if ("undefined" !== typeof canvas.environment.primary.foreground) {
        if (game.settings.get("scene-image-interpolation-settings", "foreground-enable")) {
            setTextureStyle(canvas.environment.primary.foreground, game.settings.get("scene-image-interpolation-settings", "foreground-scale-mode"), game.settings.get("scene-image-interpolation-settings", "foreground-mipmap-mode"));
        } else if (useDefaults) {
            // Default values in FVTT 0.8.9.
            canvas.environment.primary.foreground.texture.baseTexture.setStyle(PIXI.SCALE_MODES.LINEAR, PIXI.MIPMAP_MODES.OFF).update();
        }
    }
};

const getScaleModes = () => {
    let modes = {
        "NEAREST": "scene-image-interpolation-settings.scale-mode.nearest",
        "LINEAR": "scene-image-interpolation-settings.scale-mode.linear"
    };

    // Automatic support for future scaling modes.
    for (let key in PIXI.SCALE_MODES) {
        if (PIXI.SCALE_MODES.hasOwnProperty(key) && isNaN(Number(key)) && !(key in modes)) {
            modes[key] = `${game.i18n.localize("scene-image-interpolation-settings.unknown-mode")} ${key}`;
        }
    }

    return modes;
};

const getMipmapModes = () => {
    let modes = {
        "OFF": "scene-image-interpolation-settings.mipmap-mode.off",
        "POW2": "scene-image-interpolation-settings.mipmap-mode.power2",
        "ON": "scene-image-interpolation-settings.mipmap-mode.on",
        "ON_MANUAL": "scene-image-interpolation-settings.mipmap-mode.on-manual"
    };

    // Automatic support for future mipmap modes.
    for (let key in PIXI.MIPMAP_MODES) {
        if (PIXI.MIPMAP_MODES.hasOwnProperty(key) && isNaN(Number(key)) && !(key in modes)) {
            modes[key] = `${game.i18n.localize("scene-image-interpolation-settings.unknown-mode")} ${key}`;
        }
    }

    return modes;
};

const registerSettings = function() {
    game.settings.register("scene-image-interpolation-settings", "environment-fxaa-enable", {
        name: "scene-image-interpolation-settings.environment-fxaa-enable-label",
        hint: "scene-image-interpolation-settings.environment-fxaa-enable-hint",
        default: false,
        type: Boolean,
        scope: "client",
        config: true,
        onChange: value => {
            setEnvironmentFXAA(value);
        }
    });

    game.settings.register("scene-image-interpolation-settings", "background-enable", {
        name: "scene-image-interpolation-settings.background-enable-label",
        hint: "scene-image-interpolation-settings.background-enable-hint",
        default: true,
        type: Boolean,
        scope: "client",
        config: true,
        onChange: value => {
            updateSceneImageRenderingSettings(!value);
        }
    });

    game.settings.register("scene-image-interpolation-settings", "background-scale-mode", {
        name: "scene-image-interpolation-settings.scale-mode-label",
        hint: "scene-image-interpolation-settings.scale-mode-hint",
        scope: "client",
        config: true,
        type: String,
        choices: getScaleModes(),
        default: "NEAREST",
        onChange: value => {
            if (canUpdateBackground()) {
                setBackgroundScaleMode(value);
            }
        }
    });

    game.settings.register("scene-image-interpolation-settings", "background-mipmap-mode", {
        name: "scene-image-interpolation-settings.mipmap-mode-label",
        hint: "scene-image-interpolation-settings.mipmap-mode-hint",
        scope: "client",
        config: true,
        type: String,
        choices: getMipmapModes(),
        default: "OFF",
        onChange: value => {
            if (canUpdateBackground()) {
                setBackgroundMipmapMode(value);
            }
        }
    });

    game.settings.register("scene-image-interpolation-settings", "foreground-enable", {
        name: "scene-image-interpolation-settings.foreground-enable-label",
        hint: "scene-image-interpolation-settings.foreground-enable-hint",
        default: true,
        type: Boolean,
        scope: "client",
        config: true,
        onChange: value => {
            updateSceneImageRenderingSettings(!value);
        }
    });

    game.settings.register("scene-image-interpolation-settings", "foreground-scale-mode", {
        name: "scene-image-interpolation-settings.scale-mode-label",
        hint: "scene-image-interpolation-settings.scale-mode-hint",
        scope: "client",
        config: true,
        type: String,
        choices: getScaleModes(),
        default: "NEAREST",
        onChange: value => {
            if (canUpdateForeground()) {
                setForegroundScaleMode(value);
            }
        }
    });

    game.settings.register("scene-image-interpolation-settings", "foreground-mipmap-mode", {
        name: "scene-image-interpolation-settings.mipmap-mode-label",
        hint: "scene-image-interpolation-settings.mipmap-mode-hint",
        scope: "client",
        config: true,
        type: String,
        choices: getMipmapModes(),
        default: "OFF",
        onChange: value => {
            if (canUpdateForeground()) {
                setForegroundMipmapMode(value);
            }
        }
    });
};

Hooks.once("init", () => {
    registerSettings();
});

Hooks.on("canvasReady", () => {
    updateSceneImageRenderingSettings(false);
});
