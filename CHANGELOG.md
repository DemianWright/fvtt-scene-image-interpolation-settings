# Release notes

## 4.0.0

* Support for Foundry VTT versions from 10.260 to 11.307.
* Add support for disabling fast approximate anti-aliasing (FXAA) on the background and foreground images that caused pixels to be blurry when interpolation was set to "Pixel-perfect".

## 3.0.0

* Support for Foundry VTT versions from 10.260 to 11.293.
* This module version will not function as intended starting from FVTT version 11.294 due to blurry pixels even when interpolation is set to "Pixel-perfect".

## 2.0.0

* Support for Foundry VTT versions from 0.8.2 to 9.280.

## 1.0.0

* Support for Foundry VTT 0.7.5, 0.7.6, 0.7.7, 0.7.8, 0.7.9, and 0.7.10.
* May be compatible with Foundry VTT 0.8.0 and 0.8.1 but this is untested.

## 0.2.0

* Fix manifest URLs.
* Reduce size of cover image.

## 0.1.0

* Pre-release to test CI.

##
