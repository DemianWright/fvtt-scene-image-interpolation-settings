<!--suppress HtmlDeprecatedAttribute -->
# Foundry VTT Scene Image Interpolation Settings
[![FVTT Version](https://img.shields.io/badge/dynamic/json.svg?url=https%3A%2F%2Fgitlab.com%2FDemianWright%2Ffvtt-scene-image-interpolation-settings%2F-%2Freleases%2Fpermalink%2Flatest%2Fdownloads%2Fmodule.json&label=FVTT%20Version&query=$.compatibleCoreVersion&colorB=orange)](https://foundryvtt.com/ "Foundry Virtual Tabletop website") [![Module Version](https://img.shields.io/badge/dynamic/json.svg?url=https%3A%2F%2Fgitlab.com%2FDemianWright%2Ffvtt-scene-image-interpolation-settings%2F-%2Freleases%2Fpermalink%2Flatest%2Fdownloads%2Fmodule.json&label=Module%20Version&query=$.version&color=orange)](https://gitlab.com/DemianWright/fvtt-scene-image-interpolation-settings/-/releases/permalink/latest/downloads/module.json "The latest manifest URL") [![Manifest+ Version](https://img.shields.io/badge/dynamic/json.svg?url=https%3A%2F%2Fgitlab.com%2FDemianWright%2Ffvtt-scene-image-interpolation-settings%2F-%2Freleases%2Fpermalink%2Flatest%2Fdownloads%2Fmodule.json&label=Manifest%2B%20Version&query=$.manifestPlusVersion&colorB=blue)](https://foundryvtt.wiki/en/development/manifest-plus "Manifest+ specification")
[![MIT License](https://img.shields.io/badge/License-MIT-lightgray)](https://opensource.org/licenses/MIT "MIT License")

A [Foundry Virtual Tabletop](https://foundryvtt.com/ "Foundry Virtual Tabletop website.") module that enables the user to adjust the scene background and foreground image **interpolation** and **mipmap** settings for their own client.

This module will by default adjust the rendering settings of both the background and foreground images to produce the "pixel-perfect" results shown in the images as that is the most common use-case for this module. If that is your desired look, then you can simply enable this module and forget about it. Further configuration can be done in the **Module Settings** tab of the game settings.

For a module that does this to tokens and tiles, see [Proper Pixels](https://foundryvtt.com/packages/package/proper-pixels "Foundry VTT package page for the Proper Pixels module.").

Download the newest version's archive **[scene-image-interpolation-settings.zip](https://gitlab.com/api/v4/projects/48354691/packages/generic/fvtt-scene-image-interpolation-settings/latest/scene-image-interpolation-settings.zip "Direct download link for the module's newest ZIP archive.")** or install it directly in the application using the manifest URL:
```
https://gitlab.com/DemianWright/fvtt-scene-image-interpolation-settings/-/releases/permalink/latest/downloads/module.json
```
<img src="img/cover.png" alt="Module cover image." width="50%"/>

## Features
<img src="img/settings.png" alt="Module settings." width="33%" align="right"/>

Uses the PixiJS (Foundry VTT renderer) [setStyle](https://api.pixijs.io/@pixi/core/PIXI/BaseTexture.html#setStyle "PixiJS API documentation page about the setStyle function.") function to change the pixel interpolation ("scale mode") and mipmap filter settings of the scene foreground and background images separately. This module will automatically support new interpolation and filtering modes added by newer versions of PixiJS, with the caveat that the new options will not be translated in the dropdown menus.

### Settings
* **Enable Scene Image FXAA**: turn on the [fast approximate anti-aliasing](https://en.wikipedia.org/wiki/Fast_approximate_anti-aliasing "English Wikipedia page for FXAA.") (FXAA) algorithm for both the background, and the foreground images. This will blur the pixels slightly, which may be a desired look when interpolation is set to **Smooth (linear)**. This option should *not* be used in combination with the **Pixel-perfect (nearest-neighbor)** interpolation mode as the pixels will appear blurry and grainy, instead of crisp and clean, defeating the purpose of using that interpolation mode. FXAA for scene images is enabled by default in Foundry VTT but disabled by default by this module.

![An image split into four quadrants showing how a scene image looks with different combinations of the FXAA and interpolation settings.](img/fxaa-interpolation-comparison.png)

### Interpolation modes
The interpolation method used to blend adjacent pixel colors at all times. Especially noticeable at high zoom levels (nearby).

* **Pixel-perfect (nearest-neighbor)**: recommended for digital art, digital drawings, and other images with strong contrasting lines and simple colors (e.g., battlemaps, pixel art). Results in sharp pixels drawn exactly like they are in the original source image. This is the *default option* for this module and will automatically be selected.
* **Smooth (linear)**: the default Foundry VTT interpolation. Results in smooth slightly blurred images.

### Mipmap filtering modes
The mipmap filtering method used to blend pixel colors when zooming. Especially noticeable at low zoom levels (far away).

* **Off**: no mipmap filtering. Results in sharper images. This is the *default option* for this module and Foundry VTT and will automatically be selected.
* **Generated**: the game will automatically generate mipmap images. Results in smoother and blurrier images at low zoom levels. Mipmaps generated for images that do not have dimensions that are powers of two are slightly worse in quality.
* **Generated (if power of 2 dimensions)**: the game will automatically generate mipmap images only if both image dimensions are powers of two, otherwise mipmap filtering will be set to **Off** for that image.
* **Use image mipmaps**: use mipmap data from the image file itself. This means you're probably dealing with slightly more esoteric image formats such as [.DDS](http://wiki.polycount.com/wiki/DDS "Polycount wiki page about the DDS texture container file format."). In some Foundry VTT versions the scene image will turn black if the image file does not contain mipmaps.

## Screenshot
The sections labeled **old** show how Foundry VTT renders the background image by default, the sections labeled **new** show how the image renders by default after installing this module. Click the image to view it at full resolution.

![A comparison screenshot showing a zoomed out section of a battlemap.](img/preview.png)

## Thanks
The battlemap featured in the images is [Monastery Battlemap by gurkenlabs](https://gurkenlabs.itch.io/monastery-battlemap "Itch.io page of the battlemap.").
